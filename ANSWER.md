1 - analyse du projet GCP - météo

API: 

BULK INSERT:
Utilisation du yield pour permettre de commencer l'écriture dans big querry ligne par ligne sans avoir attendre la fin de la lecture des réponses dans list_station_telemetry. Idéalement à remplacer par un bulk insert une fois les résultats de la requête bien reçu.
voir: https://cloud.google.com/bigquery/docs/write-api-batch-load#python

CLOUD FUNCTION:
Il peut être intéressant de passer sur une lambda/cloud functions pour réduire la partie terraform/docker - il sera toujours nécessaire de terraformer la lambda/cloud functions. Le timeout maximun d'une cloud function est de 60 min vs 15 pour les lambdas voir le temps d'exec actuel.

CONCURRENCY:
Instead of using a cron we can use an airflow to trigger the python job each 15 min and one job for each station. This will parallelis the data ingestion and reduce the global writing time. Bigquerry can handle multiple writers on the same table with APPEND so it should be fine.

LOAD BUCKET INTO BIG QUERRY
En fonction du temps de processing de l'import de base la partie python docker peut être conservée mais allégée pour faire des imports globaux lors de l'ajout d'une nouvelle station ou de la récupération de données.
Il est néanmoins possible de charger les données d'un bucket directement dans bigquerry sans avoir à extraire les données du json: https://cloud.google.com/bigquery/docs/loading-data-cloud-storage-json

AUTRES: 
Si l'on a besoin de garder les données brut pour faire des traitements dessus il est également possible de la stocker sur s3 en // de big querry
On peut ainsi dans notre cas réécrire des données en base sans avoir à appeler l'API une nouvelle fois (pour rejouer des scénarios ou en cas de perte de données)
Il serait également intéressant de nettoyer les données afin de simplifier les requêtes et la visualisation - enlever les lignes sans dates par exemple, etc. 

BIG QUERRY:

VIEW:
Créer des views sur les données les plus requêtées avec tout les données accessibles (à plat si possible) même si il y a de la redondance de données https://cloud.google.com/bigquery/docs/views

Après réalisation de la partie requête il peut être intéressant de nettoyer les données avant insertion en base - supprimer les duplicats, les valeurs nulles etc.
On peut aussi créer des nouvelles columns qui seront utilisées régulièrements (year, week, etc.)
Il est envisageable également de créer une vue par station/jour avec la moyenne des valeurs et potentiellement les données des jours suivants sur la même row (cf requêtes avec LAG)



SQL:
Total row: 3899901
heure de paris is not null: 3888773
heure_utc is not null: 3888777
timestamp is not null: 420006

use heure_de_paris

Combien y a-t-il d’enregistrements en 2021 ?
SELECT COUNT(*) AS nb_data_2021
FROM `ds_toulouse_meteo_data.tb_telemetry`
WHERE EXTRACT(YEAR FROM heure_de_paris) = 2021;
    
--> 1200082

Pourcentage de stations qui ont été mise à jour hier
-> COUNT OVER the partition defined by max group by
SELECT
  100*(SELECT COUNT(*) OVER ()
  FROM `ds_toulouse_meteo_data.tb_telemetry`
  WHERE EXTRACT(DAYOFYEAR FROM heure_de_paris) = EXTRACT(DAYOFYEAR FROM DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY))
  AND EXTRACT(YEAR FROM heure_de_paris) = EXTRACT(YEAR FROM DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY))
  GROUP BY station_id LIMIT 1) / 
  COUNT(DISTINCT(slug)) as percentage_update_yesterday
FROM `ds_toulouse_meteo_data.tb_stations`

--> 6.1224489795918364


Liste des stations (titre, description) dont la pression max en 2021 a été inférieure à 100000.
WITH max_p AS (
  SELECT s.slug AS slug, s.title AS title, s.description AS description, MAX(t.pression) AS max_pression 
  FROM `ds_toulouse_meteo_data.tb_stations` AS s, `ds_toulouse_meteo_data.tb_telemetry` AS t
  WHERE s.slug = t.station_id
  AND EXTRACT(YEAR FROM t.heure_de_paris) = 2021
  GROUP BY title, description, slug
)
SELECT title, description, max_pression FROM max_p WHERE max_p.max_pression < 100000;

--> 50 Station météo Blagnac Quinze Sols with 90000


Liste des station_id de la table télémétries (tb_telemetry) qui ne sont pas référencés dans la table référentiel des stations (tb_stations)
SELECT DISTINCT(t.station_id) 
FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
LEFT JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
WHERE s.slug IS NULL;


-->	00-station-meteo-toulouse-valade-copy0
    48-station-meteo-toulouse-la-machine-af
    39-station-meteo-tournefeuille-ecole	
    47-station-meteo-toulouse-la-machine-tm


Moyenne de la température par station la semaine dernière calendaire
SELECT s.slug, AVG(t.temperature_en_degre_c) AS avg_temp 
FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
INNER JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
WHERE EXTRACT(ISOWEEK FROM heure_de_paris) = EXTRACT(ISOWEEK FROM DATE_ADD(CURRENT_DATE(), INTERVAL -7 DAY))
AND EXTRACT(YEAR FROM heure_de_paris) = EXTRACT(YEAR FROM DATE_ADD(CURRENT_DATE(), INTERVAL -7 DAY))
GROUP BY s.slug;

--> 28-station-meteo-toulouse-carmes 8.043452380952381
    42-station-meteo-toulouse-parc-compans-cafarelli 7.2615499254843483
    02-station-meteo-toulouse-marengo 7.5547619047619055
    21-station-meteo-cugnaux-general-de-gaulle 14.388571428571


A quelle heure la pression atmosphérique est-elle au minimum en général ?
SELECT EXTRACT(HOUR FROM heure_de_paris) AS heure, AVG(pression) AS avg_pression 
FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
INNER JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
WHERE heure_de_paris IS NOT NULL
GROUP BY heure
ORDER BY avg_pression ASC
LIMIT 1;

--> 18h avec 99323.332039823392


Temps moyen entre 2 mesures par station
WITH lag_update AS (
  SELECT slug, heure_de_paris,
  LAG(heure_de_paris) OVER (PARTITION BY slug ORDER BY heure_de_paris) AS last_update
  FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
  INNER JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
  WHERE heure_de_paris IS NOT NULL
)
SELECT slug, ROUND(AVG(DATETIME_DIFF(heure_de_paris, last_update, SECOND)), 0) AS avg_time_btw_update FROM lag_update
WHERE last_update IS NOT NULL
GROUP BY slug;

--> 14-station-meteo-toulouse-centre-pierre-potier 1209.0
    34-station-meteo-toulouse-teso 1131.0
    26-station-meteo-toulouse-reynerie 1430.0
    01-station-meteo-toulouse-meteopole 922.0


Donner la moyenne de la température, le premier mois d’exportation pour chaque station
WITH import_date AS (
  SELECT slug, MIN(heure_de_paris) AS min_date,
  FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
  INNER JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
  GROUP BY slug
),
get_year_month AS (
  SELECT slug, EXTRACT(YEAR FROM min_date) AS year_import, EXTRACT(MONTH FROM min_date) AS month_import
  FROM import_date
)
SELECT slug, ROUND(AVG(temperature_en_degre_c), 1) as avg_temp 
FROM get_year_month
INNER JOIN `ds_toulouse_meteo_data.tb_telemetry` AS t ON slug = t.station_id
WHERE EXTRACT(YEAR FROM heure_de_paris) = year_import
AND EXTRACT(MONTH FROM heure_de_paris) = month_import
GROUP BY slug;

--> 13-station-meteo-toulouse-pech-david 27.2
    14-station-meteo-toulouse-centre-pierre-potier 19.3
    09-station-meteo-toulouse-la-salade 29.2
    22-station-meteo-colomiers-za-perget 20.3


Donner la somme cumulée de la pluie sur une fenêtre glissante de 3 jours (précédents), pour chaque jours et chaque station de la semaine n°5 de l’année 2021
WITH get_days_year AS (
  SELECT slug, 
    CONCAT(EXTRACT(YEAR FROM heure_de_paris),'_',FORMAT("%03d",EXTRACT(DAYOFYEAR FROM heure_de_paris))) AS year_dayofyear,
    ROUND(AVG(pluie),2) AS avg_pluie
  FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
  INNER JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
  WHERE heure_de_paris IS NOT NULL
  GROUP BY slug, year_dayofyear
),
window_3_last_days AS (
  SELECT slug, year_dayofyear, avg_pluie,
    LAG(avg_pluie) OVER w AS avg_pluie_d2,
    LAG(avg_pluie, 2) OVER w AS avg_pluie_d3
  FROM get_days_year
  WINDOW w AS (ORDER BY slug, year_dayofyear)
  ORDER BY slug, year_dayofyear
)
SELECT slug, (avg_pluie + avg_pluie_d2 + avg_pluie_d3) AS three_last_day_rain, year_dayofyear FROM window_3_last_days

--> Manque le filtre sur l'année et la semaine TBD


Donner la liste des plages de jours consécutifs de canicules (température supérieur à 25 degré toute la journée) pour chacune des stations
WITH get_temp_days AS (
  SELECT slug, 
    CONCAT(EXTRACT(YEAR FROM heure_de_paris),'_',FORMAT("%03d",EXTRACT(DAYOFYEAR FROM heure_de_paris))) AS year_dayofyear,
    ROUND(AVG(temperature_en_degre_c),2) AS avg_temp
  FROM `ds_toulouse_meteo_data.tb_telemetry` AS t
  INNER JOIN `ds_toulouse_meteo_data.tb_stations` AS s ON s.slug = t.station_id
  WHERE heure_de_paris IS NOT NULL
  GROUP BY slug, year_dayofyear
),
temp_next_day AS (
  SELECT slug, year_dayofyear, avg_temp,
    LEAD(avg_temp) OVER w AS avg_temp_next,
  FROM get_temp_days
  WINDOW w AS (ORDER BY slug, year_dayofyear)
  ORDER BY slug, year_dayofyear
),
get_consecutive_days AS (
  SELECT 
    slug, year_dayofyear, avg_temp,
    ROW_NUMBER() OVER (PARTITION BY slug ORDER BY year_dayofyear) - 
    ROW_NUMBER() OVER (PARTITION BY slug, CASE WHEN avg_temp >= 25 THEN 1 ELSE 0 END ORDER BY year_dayofyear) AS grp
  FROM temp_next_day
)
SELECT 
  slug, MIN(year_dayofyear) AS start_day, MAX(year_dayofyear) AS end_day
FROM get_consecutive_days
WHERE avg_temp >= 25
GROUP BY slug, grp
ORDER BY slug, start_day;

--> Un poil complexe: basée sur la function ROW_NUMBER qui permet de récupérer le numéro de ligne en fonction du partition by defini, dans notre cas on retourne l'id order by date mais également en fonction de la temperature. 

ROW_NUMBER() OVER (PARTITION BY slug, CASE WHEN avg_temp >= 25 THEN 1 ELSE 0 END ORDER BY year_dayofyear) -> attribue un numéro de ligne unique à chaque ligne du résultat, en partitionnant les données par id et la temp > 25 et order by year_dayofyear
ROW_NUMBER() OVER (PARTITION BY slug ORDER BY year_dayofyear) -> attribue un numéro de ligne unique partionné sur l'id et order by year_dayofyear
En soustraillant les deux on obtient des grps de données distinct que l'on peut utiliser pour filtrer. 


