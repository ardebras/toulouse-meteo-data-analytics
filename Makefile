
PROJECT := toulouse-meteo-data-analytics
MODULE_NAME := toulouse-meteo-connector
BUILD_REVISION := module/revision
TF_PLAN := iac/tfplan

build-app:
	# docker build -t gcr.io/$(PROJECT)/$(MODULE_NAME) module
	docker build \
		--platform linux/amd64 \
		--tag gcr.io/$(PROJECT)/$(MODULE_NAME):latest \
		module

deploy-app:
	docker push gcr.io/$(PROJECT)/$(MODULE_NAME)

tf-plan:
	terraform -chdir=iac plan -var-file=envs/prod.tfvars -var 'module_revision=$(shell \
					gcloud container images list-tags gcr.io/$(PROJECT)/$(MODULE_NAME) \
					--quiet --filter tags=latest --format="get(digest)")' -out=$(shell basename $(TF_PLAN))

tf-deploy:
	terraform -chdir=iac apply -auto-approve -var-file=envs/prod.tfvars -var 'module_revision=$(shell \
					gcloud container images list-tags gcr.io/$(PROJECT)/$(MODULE_NAME) \
					--quiet --filter tags=latest --format="get(digest)")'

