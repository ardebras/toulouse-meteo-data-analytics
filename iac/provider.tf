provider "google" {
  project = var.project
  region  = var.region
}

terraform {
  backend "gcs" {
    bucket = "sl-tmda-deploy-eu"
    prefix = "terraform-state"
  }
}
