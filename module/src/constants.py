import os

import requests

PROJECT_ID = os.getenv("PROJECT_ID")

if not PROJECT_ID:
    PROJECT_ID = requests.get(
        "http://metadata.google.internal/computeMetadata/v1/project/project-id",
        headers={"Metadata-Flavor": "Google"},
    ).content

BACKFILL_BUCKET = os.environ["BACKFILL_BUCKET"]

STATIONS_LIST = [
    "02-station-meteo-toulouse-marengo",
    "09-station-meteo-toulouse-la-salade",
    "28-station-meteo-toulouse-carmes",
    "14-station-meteo-toulouse-centre-pierre-potier",
    "11-station-meteo-toulouse-soupetard",
    "42-station-meteo-toulouse-parc-compans-cafarelli",
    "53-station-meteo-toulouse-ponsan",
    "36-station-meteo-toulouse-purpan",
    "20-station-meteo-mondonville-ecole",
    "19-station-meteo-mondouzil-mairie",
    "23-station-meteo-pibrac-bouconne-centre-equestre",
    "31-station-meteo-mons-station-epuration",
    "50-station-meteo-blagnac-quinze-sols",
    "61-station-meteo-blagnac-mairie",
    "26-station-meteo-toulouse-reynerie",
    "65-station-meteo-toulouse-life-gastou",
    "48-station-meteo-toulouse-la-machine-af",
    "21-station-meteo-cugnaux-general-de-gaulle",
    "63-station-meteo-toulouse-life-marechal-juin",
    "66-station-meteo-toulouse-life-coubertin",
    "51-station-meteo-toulouse-lardenne",
    "39-station-meteo-tournefeuille-ecole",
    "05-station-meteo-toulouse-life-hall-1",
    "08-station-meteo-toulouse-basso-cambo",
    "07-station-meteo-toulouse-avenue-de-grande-bretagne",
    "22-station-meteo-colomiers-za-perget",
    "41-station-meteo-toulouse-avenue-de-casselardit",
    "24-station-meteo-colomiers-zi-enjacca",
    "03-station-meteo-toulouse-busca",
    "13-station-meteo-toulouse-pech-david",
    "62-station-meteo-toulouse-parc-maourine",
    "37-station-meteo-toulouse-universite-paul-sabatier",
    "00-station-meteo-toulouse-valade-copy",
    "00-station-meteo-toulouse-valade-copy0",
    "00-station-meteo-toulouse-valade",
    "49-station-meteo-toulouse-cote-pavee",
    "40-station-meteo-toulouse-zi-thibaud",
    "01-station-meteo-toulouse-meteopole",
    "25-station-meteo-tournefeuille-residentiel",
    "47-station-meteo-toulouse-la-machine-tm",
    "04-station-meteo-toulouse-ile-empalot",
    "10-station-meteo-castelginest-ecole",
    "05-station-meteo-toulouse-nakache",
    "18-station-meteo-brax-ecole",
    "17-station-meteo-fenouillet-foyer",
    "58-station-meteo-toulouse-fondeyre",
    "32-station-meteo-mons-ecole",
    "27-station-meteo-toulouse-saint-cyprien",
    "12-station-meteo-toulouse-montaudran",
    "34-station-meteo-toulouse-teso",
    "06-station-meteo-toulouse-ecluse",
    "15-station-meteo-l-union-ecole",
    "30-station-meteo-toulouse-george-sand",
    "38-station-meteo-toulouse-parc-jardin-des-plantes",
    "33-station-meteo-saint-jory-chapelle-beldou",
    "45-station-meteo-toulouse-st-exupery",
]
