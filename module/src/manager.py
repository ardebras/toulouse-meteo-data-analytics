from typing import Optional

from datetime import datetime

from constants import STATIONS_LIST, BACKFILL_BUCKET
from bigquery_helper import BigQueryHelper
from storage_helper import StorageHelper
from service import Service

DST_TABLE = {
    "project": "toulouse-meteo-data-analytics",
    "dataset": "ds_toulouse_meteo_data",
    "table": "tb_telemetry",
}


class Manager:
    def __init__(
        self, service: Service, bq_writer: BigQueryHelper, storage_helper: StorageHelper
    ):
        self.service = service
        self.bq_writer = bq_writer
        self.uploader = storage_helper

    def get_last_inserted_value_ts(self, station: Optional[str] = None) -> str:
        """Return the last inserted value timestamp for this station."""
        return self.bq_writer.get_max_timestamp(DST_TABLE, station)

    def _extract_all_of(self, station: str):
        try:
            telemetry_file = self.service.export_station_telemetry(station)
            self.uploader.upload(BACKFILL_BUCKET, telemetry_file)
        except Exception as e:
            print(e)

    def extract_all(self, station: str = None):
        if station:
            self._extract_all_of(station)
        else:
            for st in STATIONS_LIST:
                self._extract_all_of(st)

    def _extract_new_of(self, station: str, from_ts: datetime):
        """Extract and process telemetry of one station from a point in time."""
        telemetry = self.service.list_station_telemetry(station, from_ts=from_ts)
        # to do create a list of record to insert & then create a bulk insert? 
        for record in telemetry:
            self.bq_writer.append_data(
                data={**record, **{"station_id": station}}, dst_table=DST_TABLE
            )

    def extract_new(self, station: str = None):
        if station:
            self._extract_new_of(station, self.get_last_inserted_value_ts(station))
        else:
            for st in STATIONS_LIST:
                self._extract_new_of(st, self.get_last_inserted_value_ts(st))
