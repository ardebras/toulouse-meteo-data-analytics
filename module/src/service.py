from typing import List, Dict, Any, Optional
from datetime import datetime

from connector import Connector

MAX_RESULT_PAGE = 100


class Service:
    def __init__(self, connector: Connector):
        self.connector = connector

    def list_station_telemetry( self, station: str, 
                from_ts: Optional[datetime] = None) -> List[Dict[str, Any]]:
        params = {
            "limit": str(MAX_RESULT_PAGE),
            "timezone": "UTC",
        }
        if from_ts:
            params["where"] = f"heure_de_paris>date'{from_ts.isoformat()}'"

        last_page = False
        i = 1
        while not last_page:
            data = self.connector.get(
                endpoint=f"catalog/datasets/{station}/records", params=params
            ).json()
            for record in data.get("records", []):
                # return generator object and will continue to execute the code before continuing the for yield
                yield record.get("record")

            params.update({"offset": str(i * MAX_RESULT_PAGE)})
            i += 1
            last_page = (
                len(list(filter(lambda l: l["rel"] == "next", data.get("links", []))))
                == 0
            )

    def export_station_telemetry(self, station: str):
        local_filename = f"{station}.jsonl"
        params = {
            "timezone": "UTC",
            "select": (
                "data, humidite, direction_du_vecteur_de_vent_max, pluie_intensite_max,"
                " pression, direction_du_vecteur_vent_moyen, type_de_station, pluie,"
                " direction_du_vecteur_de_vent_max_en_degres,"
                " force_moyenne_du_vecteur_vent, force_rafale_max,"
                " temperature_en_degre_c, heure_de_paris, heure_utc"
            ),
        }
        with self.connector.get(
            f"catalog/datasets/{station}/exports/jsonl", params=params, stream=True
        ) as resp:
            resp.raise_for_status()
            with open(local_filename, "wb") as localf:
                for chunk in resp.iter_content(chunk_size=8192):
                    # If you have chunk encoded response uncomment if
                    # and set chunk_size parameter to None.
                    # if chunk:
                    localf.write(chunk)
        return local_filename
